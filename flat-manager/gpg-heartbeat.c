#include <gpgme.h>

int main (int argc, char **argv) {
  char *homedir, *key_id;
  gpgme_ctx_t context = NULL;
  gpgme_engine_info_t info;
  gpgme_key_t key = NULL;

  if (argc != 3) {
    printf("Usage: %s homedir key_id\n", argv[0]);
    return 1;
  }

  homedir = argv[1];
  key_id = argv[2];

  gpgme_check_version (NULL);

  if (gpgme_new (&context) != GPG_ERR_NO_ERROR)
    {
      fprintf (stderr, "Unable to create gpg context\n");
      return 1;
    }


  info = gpgme_ctx_get_engine_info (context);

  if (gpgme_ctx_set_engine_info (context, info->protocol, NULL, homedir) != GPG_ERR_NO_ERROR)
    {
      fprintf (stderr, "Unable to set gpg homedir to '%s'", homedir);
      return 1;
    }


  if (gpgme_get_key (context, key_id, &key, 1) != GPG_ERR_NO_ERROR)
    {
      fprintf (stderr, "Unable to lookup key ID %s", key_id);
      return 1;
    }

  return 0;
}
