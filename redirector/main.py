import os.path

import requests
from fastapi import FastAPI, Response, status
from fastapi.responses import RedirectResponse
from pydantic import BaseSettings


class Settings(BaseSettings):
    s3_url: str = "https://gnome-build-meta.s3.amazonaws.com"
    cdn_url: str = "https://1270333429.rsc.cdn77.org"
    mirror_url: str = "https://download.gnome.org/gnomeos"
    volume_path: str = "/ftp/pub/GNOME/gnomeos"
    main_url: str = "https://os.gnome.org"


cfg = Settings()
app = FastAPI()


@app.get("/status")
def healthcheck():
    return {"status": "OK"}


@app.get("/latest/{filename}")
def get_nightly(filename: str):
    if filename.endswith(".iso"):
        latest_filename = "latest-iso"
    elif filename.endswith(".img.xz"):
        latest_filename = "latest-disk"
    else:
        return Response(status_code=status.HTTP_404_NOT_FOUND)

    r = requests.get(f"{cfg.s3_url}/{latest_filename}")
    latest = r.text.rstrip()
    _, version, filename = latest.split("/")

    response = RedirectResponse(f"{cfg.main_url}/download/{version}/{filename}")
    response.status_code = status.HTTP_302_FOUND
    return response


@app.get("/{version}/{filename}")
def get_versioned(version: str, filename: str):
    filepath = f"{cfg.volume_path}/{version}/{filename}"

    # Redirect to mirror if file is available in the FTP volume
    if os.path.isfile(filepath):
        response = RedirectResponse(f"{cfg.mirror_url}/{version}/{filename}")
        response.status_code = status.HTTP_302_FOUND
        return response

    # Otherwise, redirect to CDN/S3
    if "." in version:
        folder = "tag"
    else:
        folder = "nightly"

    response = RedirectResponse(f"{cfg.cdn_url}/{folder}/{version}/{filename}")
    response.status_code = status.HTTP_302_FOUND
    return response
